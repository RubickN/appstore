﻿function loadpage() {
    $.jqPaginator('#pagination', {
        totalPages: parseInt($("#PageCount").val()),
        visiblePages: parseInt($("#visiblePages").val()),
        currentPage: parseInt($("#currentPage").val()),
        first: '<li class="first"><a style="margin: 5px;" href="javascript:;">首页</a></li>',
        prev: '<li class="prev"><a style="margin: 5px;" href="javascript:;"><i class="arrow arrow2"></i>上一页</a></li>',
        next: '<li class="next"><a style="margin: 5px;" href="javascript:;">下一页<i class="arrow arrow3"></i></a></li>',
        last: '<li class="last"><a style="margin: 5px;" href="javascript:;">末页</a></li>',
        page: '<li class="page"><a style="margin: 5px;" href="javascript:;">{{page}}</a></li>',
        onPageChange: function (num, type) {
            if (type == "change") {
                search(num);
            }
        }
    });
}
loadpage();