from flask import Blueprint
from flask import render_template
import requests
import json
from flask import jsonify, request
index_bp = Blueprint('index', __name__, static_folder='/')
AGAHNIM_URL = "http://127.0.0.1:8088"


@index_bp.route('/app/index')
def index():
    current_date_url = AGAHNIM_URL + "/searcher/latest_date"
    current_date = requests.get(current_date_url)
    current_resource_num_url = AGAHNIM_URL + "/searcher/resource_num"
    current_resource_num = requests.get(current_resource_num_url)

    return render_template(
        'index.html',
        latest_date=json.loads(current_date.text)['data']['data'],
        all_resource_num=json.loads(current_resource_num.text)['data']['data']
        )


@index_bp.route('/app/detail')
def detail():
    search_html_url = AGAHNIM_URL + "/searcher/search_html?key_word=%s&resource_id=%s"
    key_words = request.args.get('key_word')
    resource_id = request.args.get('resource_id')
    search_html_url = search_html_url % (key_words, resource_id)
    search_html = requests.get(search_html_url)
    return search_html.text
